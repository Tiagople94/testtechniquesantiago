****## Test Technique Maline

### Pré-requis

- serveur web (Wamp64, ...)
- mise à jour des vhosts (en fonction de votre environnement)
- mise à jour des hosts (exemple ``127.0.0.1 testTech``)

### Installation

Veuillez finir d'installer le projet :
``$ composer install``

## Démarrage

Démarrer le serveur
``php -S localhost:8000 -t public
``

Rendez-vous sur la page d'accueil. Vous devriez voir apparaître : 
Une page en blanc avec un title "Liste d'Appartements!"

## Methode pour recuperer le test 

Pour recuperer le test technique:
1. Faire une git clone en local sur URL: https://gitlab.com/Tiagople94/testtechniquesantiago.git
2. Configurer le .env pour connecter l'application a une BDD

## Reste à faire

- ATTENTION: Ajouter directement sur la table "Flat" les données concernant un Appartement.
  
- A part ceci,sauf erreur de comprehension de ma part, le besoin fonctionnel demandé est realisé a 100%.
  RAS d'autre pour ma part.

## Temps Total passé sur le test

- Environ 3 jours (Les apres-midi/Soirs)

## Pistes d'amélioration dans le travail réalisé

1. FrontEnd: 
  - Amelioration du rendu visuelle (Utilisation d'une modale pour afficher le formulaire pour un aspect plus SPA, MAJ des données par Ajax...)
  - Integration de Framework CSS Bootstrap (Actuellement ajouté en dur sur le Base.html.twig)
2. BackEnd:
    - Mis en place d'un systeme d'exception contextualisé au service cree (FlatEntityManager)
    - Mis en place d'un data Transformer pour l'affichage des données sur le formulaire ou Transformation du string en Sentence Case (Par exemple) "Adresse" pour un stockage en BDD uniforme
    
