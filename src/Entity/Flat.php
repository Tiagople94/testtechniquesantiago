<?php

namespace App\Entity;

use App\Repository\FlatRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass=FlatRepository::class)
 */
class Flat
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Regex("/^([0-9 ]{1,2}|[a-zA-Z ])*[a-zA-Z]$/", message="Erreur dans le format de l'adresse")
     */
    private ?string $Address;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     * @Assert\Regex("/^[1-9]{1,2}$/", message="L'appartement doit appartenir a un etage entre 1-99")
     */
    private ?int $Floor;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     * @Assert\Regex("/^[1-9]{1}$/", message="L'appartement doit contenir un numero de chambres entre 1-9")
     */
    private ?int $Room_number;

    /**
     * @ORM\Column(type="boolean")
     */
    private ?bool $Elevator;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAddress(): ?string
    {
        return $this->Address;
    }

    public function setAddress(string $Address): self
    {
        $this->Address = $Address;

        return $this;
    }

    public function getFloor(): ?int
    {
        return $this->Floor;
    }

    public function setFloor(int $Floor): self
    {
        $this->Floor = $Floor;

        return $this;
    }

    public function getRoomNumber(): ?int
    {
        return $this->Room_number;
    }

    public function setRoomNumber(int $Room_number): self
    {
        $this->Room_number = $Room_number;

        return $this;
    }

    public function getElevator(): ?bool
    {
        return $this->Elevator;
    }

    public function setElevator(bool $Elevator): self
    {
        $this->Elevator = $Elevator;

        return $this;
    }
}
