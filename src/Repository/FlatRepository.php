<?php

namespace App\Repository;

use App\Entity\Flat;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Flat|null find($id, $lockMode = null, $lockVersion = null)
 * @method Flat|null findOneBy(array $criteria, array $orderBy = null)
 * @method Flat[]    findAll()
 * @method Flat[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FlatRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Flat::class);
    }

    public function findByAddress(Flat $flatEntity)
    {
        return $this->createQueryBuilder('flat')
            ->andWhere('flat.Address = :address')
            ->andWhere('flat.id != :id')
            ->setParameter('address', $flatEntity->getAddress())
            ->setParameter('id', $flatEntity->getId())
            ->orderBy('flat.id', 'ASC')
            ->getQuery()
            ->getResult();
    }
}
