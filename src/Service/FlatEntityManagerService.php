<?php
namespace App\Service;

use App\Entity\Flat;
use App\Repository\FlatRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Config\Definition\Exception\Exception;

class FlatEntityManagerService
{
    private FlatRepository $flatRepository;
    private EntityManagerInterface $em;

    public function __construct(FlatRepository $flatRepository, EntityManagerInterface $em)
    {
        $this->flatRepository = $flatRepository;
        $this->em = $em;
    }

    /**
     * Met a jour la table "Flat" en BDD
     * @param Flat $flatEntity
     * @return void
     */
    public function updateTable(Flat $flatEntity)
    {
        // Checker si champs adresse est existante en BDD
        if ($this->checkIfAddressExists($flatEntity)) {
            throw new Exception('Appartement deja existant en BDD');
        }

        //MAJ des données
        $this->em->flush();
    }

    /**
     * Recupere un Appartement selon son ID
     * @param int $id
     * @return Flat
     */
    public function getFlatById(int $id): Flat
    {
        $flat = $this->flatRepository->find($id);

        if(!$flat) {
            throw new Exception('Appartement non trouvé en BDD');
        }

        return $flat;
    }

    /**
     * Verifie si le contenu du champs "Address" existe deja en BDD
     * @param Flat $flatEntity
     * @return bool
     */
    public function checkIfAddressExists(Flat $flatEntity): bool
    {
        return !empty($this->flatRepository->findByAddress($flatEntity));
    }
}