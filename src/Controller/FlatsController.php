<?php

namespace App\Controller;

use App\Entity\Flat;
use App\Form\FlatInfoType;
use App\Service\FlatEntityManagerService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FlatsController extends AbstractController
{
    /**
     * @Route("/", name="flats")
     */
    public function index(EntityManagerInterface $em): Response
    {
        $flatsList = $em->getRepository(flat::class)->findAll();

        return $this->render('flats/index.html.twig', [
            'controller_name' => 'Liste d\'Appartements',
            'flats_list' => $flatsList,
        ]);
    }

    /**
     * @Route("/updateInfo/{id}", name="updateInfo", requirements={"id"="\d+"})
     * @param Request $request
     * @param FlatEntityManagerService $flatEntityManager
     * @return Response
     */
    public function updateFlatInfo(Request $request, FlatEntityManagerService $flatEntityManager): Response
    {
        // Recuperation de l'appartement en BDD
        $flat = $flatEntityManager->getFlatById($request->get('id'));

        // Creation du Form
        $form = $this->createForm(FlatInfoType::class, $flat);

        $form->handleRequest($request);

        // Si le Form est Posté
        if ($form->isSubmitted() && $form->isValid()) {
            $formData = $form->getData();
            // Appel au service qui MAJ les données en BDD
            try {
                $flatEntityManager->updateTable($formData);
                $this->addFlash('success', 'Mise a Jour completé');

                return $this->redirectToRoute('flats', [], 301);
            } catch (Exception $exception) {
                $this->addFlash('error', $exception->getMessage());
            }
        }

        return $this->render('flats/updateForm.html.twig', [
            'update_form' => $form->createView(),
        ]);
    }
}
