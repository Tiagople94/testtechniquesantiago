<?php

namespace App\Services;

use App\Entity\Flat;
use App\Repository\FlatRepository;
use App\Service\FlatEntityManagerService;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Config\Definition\Exception\Exception;

class FlatEntityManagerTest extends TestCase
{
    private Flat $flat;
    private FlatRepository $flatRepository;
    private EntityManagerInterface $em;

    public function setUp(): void
    {
        $this->flat = new Flat();
        $this->flatRepository = $this->createMock(FlatRepository::class);
        $this->em = $this->createMock(EntityManagerInterface::class);
    }

    public function test_Get_Flat_By_Id_Successfully()
    {
        // Given: Un Repository mocké et un Service Instancié
        $this->flatRepository->expects($this->once())
           ->method('find')
           ->willReturn($this->flat);

       $flatEM = new FlatEntityManagerService($this->flatRepository, $this->em);

        // When : Appel a une methode du Service
        // Assert: Un Retour d'une classe
        $this->assertInstanceOf(Flat::class, $flatEM->getFlatById(1));
    }

    public function test_Get_Flat_By_Id_Error_Not_Flat_Found_In_DataBase()
    {
        // Given: Un Repository mocké et un Service Instancié
        $this->flatRepository->expects($this->once())
            ->method('find')
            ->willReturn([]);
        $flatEM = new FlatEntityManagerService($this->flatRepository, $this->em);

        // Assert: Un Retour d'une exception
        $this->expectException(Exception::class);

        // When : Appel a une methode du Service
        $flatEM->getFlatById(1);
    }

    public function test_Update_Table_Error_After_Check_If_Address_Exists()
    {
        $this->expectException(Exception::class);

        // Mockage du service
        $mockFEM = $this->getMockBuilder(FlatEntityManagerService::class)
            ->setConstructorArgs([$this->flatRepository, $this->em])
            ->onlyMethods(['checkIfAddressExists'])
            ->getMock();

        // Stubage de la methode dependante
        $mockFEM->expects($this->once())
            ->method('checkIfAddressExists')
            ->will($this->returnValue(true));

        $mockFEM->updateTable($this->flat);
    }

    public function test_Update_Table_Address_Already_Exists_In_Database()
    {
        // Given: Un Service
        $flatEM = new FlatEntityManagerService($this->flatRepository, $this->em);

        // When: On appel a une methode
        $flatEM->checkIfAddressExists($this->flat);

        //Assert: Un retour Boolean
        $this->assertIsBool($flatEM->checkIfAddressExists($this->flat));
    }
}
