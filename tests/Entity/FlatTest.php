<?php

namespace App\Tests\Entity;

use App\Entity\Flat;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;


class FlatTest extends KernelTestCase
{
    private Flat $flat;

    public function setUp(): void
    {
        $this->flat = new Flat();
    }

    public function test_Valid_Flat_Entity_Successful()
    {
        $this->flat->setAddress('Rue Pepe')
                            ->setRoomNumber(1)
                            ->setElevator(true)
                            ->setFloor(2);

        $this->assertHasNotErrors($this->flat);
    }

    public function test_Special_Characters_In_Address_Error()
    {
        $this->flat->setAddress('@Rue Pepe ')
                            ->setRoomNumber(1)
                            ->setElevator(true)
                            ->setFloor(12);

        $this->assertHasErrors($this->flat, 1);
    }

    public function testRoom_Number_Can_Not_Be_Zero()
    {
        $this->flat->setAddress('Rue Pepe')
            ->setRoomNumber(0)
            ->setElevator(true)
            ->setFloor(1);

        $this->assertHasErrors($this->flat, 1);
    }

    public function testRoom_Number_Can_Not_Have_Three_or_More_Digits()
    {
        $this->flat->setAddress('Rue Pepe')
            ->setRoomNumber(999)
            ->setElevator(true)
            ->setFloor(1);

        $this->assertHasErrors($this->flat, 1);
    }

    public function test_Floor_Can_Not_Be_Zero()
    {
        $this->flat->setAddress('Rue Pepe')
            ->setRoomNumber(3)
            ->setElevator(true)
            ->setFloor(0);

        $this->assertHasErrors($this->flat, 1);
    }

    public function test_Empty_Fields_Error()
    {
        $this->assertHasErrors($this->flat, 3);
    }

    private function assertHasNotErrors(flat $flat)
    {
        self::bootKernel();
        $error = self::$container->get('validator')->validate($flat);

        $this->assertCount(0, $error);
    }

    private function assertHasErrors(flat $flat, int $numberOfErrors = 1)
    {
        self::bootKernel();
        $error = self::$container->get('validator')->validate($flat);

        $this->assertCount($numberOfErrors, $error);
    }
}
